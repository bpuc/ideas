[colors]
grey = #585b70 
text = #cdd6f4
bg = #0d0d17 
fg = #d4d7ff

[bar/top]
override-redirect = false
bottom = false
fixed-center = true

width = 100%:-10
height = 25
line-size = 1pt

background = ${colors.bg}
foreground = ${colors.fg}

padding-right = 2
#1.5 
padding-left = 0
module-margin = 0

offset-x = 5
offset-y = 4

radius-top = 8.0
radius-bottom = 8.0

; FONTS
font-0 = "Iosevka Term:pixelsize=11;3"
font-1 = "Iosevka Term:pixelsize=7;3"
font-2 = "Iosevka Term:pixelsize=12;3"
font-3 = "Iosevka Term:pixelsize=7;2"
font-4 = "Iosevka Term:pixelsize=13;4"

; MODULES
modules-left = power bspwm updates
modules-center = date
modules-right = cpu memory filesystem backlight battery netspeed

separator = %{T2}   %{T-}
#separator-foreground = ${colors.} 

; TRAY
# tray-position = right

; If true, the bar will not shift its
; contents when the tray changes
# tray-detached = false

; Tray icon max size
# tray-maxsize = 16
# tray-background = ${colors.bg}

;offset defined as pixel value (e.g. 35) or percentage (e.g. 50%)
# tray-offset-x = 0
# tray-offset-y = 0

; Pad the sides of each tray icon
# tray-padding = 2

; Scale factor for tray clients
# tray-scale = 1

wm-restack = bspwm

dpi-x = 133
dpi-y = 133

enable-ipc = true

[module/bspwm]
type = internal/bspwm
pin-workspaces = true
inline-mode = false
enable-click = true
enable-scroll = true
reverse-scroll = true
fuzzy-match = false

ws-icon-0 = "I;%{T1}  %{T-}"
ws-icon-1 = "II;%{T1}  %{T-}"
ws-icon-2 = "III;%{T1}  %{T-}"
ws-icon-3 = "IV;%{T1}  %{T-}"
ws-icon-4 = "V;%{T1}  %{T-}"
ws-icon-5 = "VI;%{T1}  %{T-}"
ws-icon-6 = "VII;%{T1}  %{T-}"
ws-icon-7 = "VIII;%{T1}  %{T-}"
ws-icon-8 = "IX;%{T1}  %{T-}"
ws-icon-9 = "X;%{T1}  %{T-}"
ws-icon-default = %{T1}  %{T-}

format = <label-state> <label-mode>
label-monitor = %name%

label-focused = %icon%
label-focused-foreground = ${colors.text}
label-focused-background = ${colors.bg}
label-focused-underline = ${colors.text}

label-occupied = %icon%
label-occupied-padding = 0
label-occupied-foreground = ${colors.text}

label-urgent = %icon%
label-urgent-foreground = ${colors.text}

label-empty = %icon%
label-empty-foreground = ${colors.grey}
label-empty-padding = 0

;label-separator =
;label-separator-padding = 0
;label-separator-foreground = #ffb52a

[module/date]
type = internal/date
interval = 1.0
date = %{T2} %a %d %b,%{T-}
time = %{T2} %I:%M %p %{T-}

label = %date%%time%
label-font = 0
label-foreground = ${colors.text}

[module/cpu]
type = internal/cpu
interval = 2.0

label = %{T1}%{T-}%{T2} %percentage:2%%%{T-}
label-foreground = ${colors.text}

[module/memory]
type = internal/memory
interval = 2.0
format = <label>

label = %{T1} %{T-}%{T2}%mb_used%%{T-}
label-foreground = ${colors.text}

[module/power]
type = custom/text
content = %{T5}  %{T-}
content-foreground = ${colors.text}
#content-font = 4

click-left = ~/.config/scripts/powermenu.sh
click-right = ~/.config/cripts/powermenu.sh

[module/updates]
type = custom/script
interval = 900s
exec = ~/.config/polybar/scripts/checkupdates.sh
format-foreground = ${colors.text}

[module/netspeed]
type = internal/network
interface = wlp0s12f0
interval = 2.0

label-connected = "%{T3}直%{T-} %{T2}%essid%%{T-}" 
; %{T2} %down:speed% | %upspeed%%{T-}"
label-disconnected = "%{T3}睊%{T-} %{T2}Offline%{T-}"
label-connected-background = ${colors.bg}
label-connected-foreground = ${colors.text}
label-disconnected-foreground = ${colors.text}

[module/title]
type = internal/xwindow

format = <label>
fomunormat-background = ${colors.bg}
format-foreground = ${colors.text}
format-padding = 2.0

label = "%{T2}%title%%{T-}"
label-maxlen = 70

label-empty =%{T2} %{T-}
label-empty-foreground = #707880

[module/backlight]
type = internal/backlight
card = intel_backlight
enable-scroll = true
format = <label>
format-foreground = ${colors.text}
label = %{T1}%{T-} %{T2}%percentage%%%{T-}

[module/battery]
type = internal/battery
full-at = 98
battery = BAT0
adapter = ADP1
poll-interval = 5.0

format-charging = <label-charging>
label-charging = %{T2}%{T-} %{T2}%percentage%%%{T-}
label-charging-foreground = ${colors.text}

format-discharging = <ramp-capacity><label-discharging>
label-discharging = %{T2}%percentage%%%{T-}
label-discharging-foreground = ${colors.text}

label-full = %{T4}%{T-} %{T2}100%%{T-}
label-full-foreground = ${colors.text}

; BAT ICONS        Icons
ramp-capacity-0 = %{T4} %{T-}
ramp-capacity-1 = %{T4} %{T-}
ramp-capacity-2 = %{T4} %{T-}
ramp-capacity-3 = %{T4} %{T-}
ramp-capacity-4 = %{T4} %{T-}
ramp-capacity-5 = %{T4} %{T-}
ramp-capacity-0-foreground = ${colors.text}
ramp-capacity-1-foreground = ${colors.text}
ramp-capacity-2-foreground = ${colors.text}
ramp-capacity-3-foreground = ${colors.text}
ramp-capacity-4-foreground = ${colors.text}
ramp-capacity-5-foreground = ${colors.text}

[module/filesystem]
type = internal/fs

mount-0 = /
interval = 60
fixed-values = false

format-mounted = <label-mounted>
format-mounted-prefix = " "
format-mounted-prefix-foreground = ${colors.text}
format-mounted-prefix-background = ${color.text}

label-mounted = %{T2}%used% %percentage_used%%%{T-}
label-mounted-foreground = ${colors.text}

[settings]
pseudo-transparency = true
