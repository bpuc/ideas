#!/bin/bash
message="${1:-"Confirm?"}"
response=$(echo -e "No\nYes" | rofi -dmenu -i -p "$message " -font "Iosevka Term 12")

if [ "$response" = "Yes" ]; then
  exit 0;
else
  exit 1;
fi
