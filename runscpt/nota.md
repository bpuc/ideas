# IMPORTANTE:

runscpt es ligero script que te ayuda a buscar y ejecutar otros escripts desde rofi.
runscpt ya es funcional, pero no está habilitado.
Para habilitarlo asignale un nuevo keybinding en 'spectrwm.conf', 'sxhkd' o 'config.py' para spectrwm, bspwm y qtile, respectivamente.
