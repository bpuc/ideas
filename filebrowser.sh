#!/bin/bash
                               
#///////////////////////#
#        F I L E        #
#///////////////////////#
#     B R O W S E R     #
#///////////////////////#

status=true
while $status
do
	choice=$(ls -a | rofi -dmenu -i -lines 12 -p "Open" -font "Iosevka Term 13")

	key=""
	read -sn3 $key

	if (( $key -eq "^[" ))
	then
		status=false
		exit
	fi

	if [ -d $choice ]; then
		cd $choice
	else
		xdg-open $choice 
	fi

done

# We use "printf '%s\n'" to format the array one item to a line.
#choice=$(printf '%s\n' "${!options[@]}" | rofi -dmenu -i -no-show-icons -line 10 \
#                                          -p "edit" -font "Iosevka Term 13") #-no-sidebar-mode

# What to do when/if we choose a file to edit.
#if [ "$choice" ]; then
#        conf=$(printf '%s\n' "${options["${choice}"]}")
#        $MYEDITOR "$conf"
# What to do if we just escape without choosing anything.
#else
#    echo "Program terminated." && exit 0
#fi

#------------------------------------#
# original by: Dilip Chauhan         #
# Github: https://github/TechnicalDC #
#------------------------------------#
