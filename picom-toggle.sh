#!/bin/bash

# Replace picom with the editor of your choice
compositor=picom

# Turn off compositor
if pgrep -x $compositor; then
    pkill $compositor
    dunstify -u normal "Blur" "$compositor is off."
    exit
fi

# Turn on compositor
if ! pgrep -x $compositor; then
    $compositor &>/dev/null &
    dunstify -u normal "Blur" "$compositor is on."
    exit
fi
